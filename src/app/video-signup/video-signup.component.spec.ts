import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSignupComponent } from './video-signup.component';

describe('VideoSignupComponent', () => {
  let component: VideoSignupComponent;
  let fixture: ComponentFixture<VideoSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
