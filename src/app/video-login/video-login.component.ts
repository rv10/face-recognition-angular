import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../api.service";

@Component({
  selector: "app-video-login",
  templateUrl: "./video-login.component.html",
  styleUrls: ["./video-login.component.css"]
})
export class VideoLoginComponent implements AfterViewInit, OnDestroy {
  @ViewChild("video", { static: false })
  public video: ElementRef;

  @ViewChild("canvas", { static: true })
  canvas: ElementRef;

  private ctx: CanvasRenderingContext2D;

  routedata: any;
  timer = 0;

  constructor(public router: Router, public api: ApiService) {
    this.routedata = router.getCurrentNavigation().extras.state;
    if (!this.routedata) {
      router.navigateByUrl("/login");
    }
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();
        setTimeout(() => this.verify(), 1000);
      });
    }
  }

  verify() {
    this.api.verifyImage(this.routedata.username, this.capture()).subscribe(
      data => {
        console.log(data);
        if (data.status === "Mached") {
          this.router.navigateByUrl("/course");
          localStorage.setItem("user", this.routedata.username);
        } else {
          this.timer++;
          if (this.timer > 10) {
            // alert("Invalid User");
            this.router.navigateByUrl("/login");
          }
          this.verify();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  public capture() {
    this.ctx = this.canvas.nativeElement.getContext("2d");
    this.canvas.nativeElement.width = this.video.nativeElement.videoWidth;
    this.canvas.nativeElement.height = this.video.nativeElement.videoHeight;
    this.ctx.drawImage(this.video.nativeElement, 0, 0, 640, 480);
    const imageDataURL = this.canvas.nativeElement.toDataURL("image/png");
    const myFile: Blob = this.api.dataURItoBlob(imageDataURL);
    const file = new File([myFile], "imageFileName.png");
    console.log(file);
    return file;
  }

  public ngOnDestroy() {
    console.log("destroy webcam");
    this.video.nativeElement.srcObject = null;
  }

}
