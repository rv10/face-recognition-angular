import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  Inject,
  OnInit
} from "@angular/core";
import { Router } from "@angular/router";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ApiService } from "../api.service";

@Component({
  selector: "app-video-signup",
  templateUrl: "./video-signup.component.html",
  styleUrls: ["./video-signup.component.css"]
})
export class VideoSignupComponent implements AfterViewInit, OnDestroy {
  @ViewChild("video", { static: false })
  public video: ElementRef;

  routedata: any;

  constructor(
    public router: Router,
    public dialog: MatDialog,
    public api: ApiService
  ) {
    this.routedata = router.getCurrentNavigation().extras.state;
    if (!this.routedata) {
      router.navigateByUrl("/signup");
    }
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();
      });
    }
  }

  capture(): void {
    const image = this.video.nativeElement;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: "370px",
      height: "350px",
      data: { image }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
      if (result) {
        const myFile: Blob = this.api.dataURItoBlob(result);
        const file = new File([myFile], Date.now().toString() + Math.random().toString(36).substring(7) + ".png");
        console.log(file);
        // signup api
        this.api.signUp(
          this.routedata.username,
          this.routedata.firstname,
          this.routedata.lastname,
          this.routedata.email,
          this.routedata.dob,
          file
        ).subscribe(
          data => {
            console.log(data);
            if (data.status) {
              // redirect to courses page
              localStorage.setItem("user", this.routedata.username);
              this.router.navigateByUrl("/course");
            } else {
              alert("Failed to Sign Up");
              setTimeout(() => this.router.navigateByUrl("/signup"), 1000);
            }
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }

  public ngOnDestroy() {
    console.log("destroy webcam");
    if (this.video !== undefined) {
      this.video.nativeElement.srcObject = null;
    }
  }
}

@Component({
  selector: "app-confirm-dialog",
  templateUrl: "confirm-dialog.html"
})
export class ConfirmDialogComponent implements OnInit {
  @ViewChild("canvas", { static: true })
  canvas: ElementRef;

  private ctx: CanvasRenderingContext2D;
  imageDataURL: any;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    console.log(data.image);
    console.log(this.canvas);
  }

  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext("2d");
    this.canvas.nativeElement.width = this.data.image.videoWidth * 0.5;
    this.canvas.nativeElement.height = this.data.image.videoHeight * 0.5;
    this.ctx.drawImage(this.data.image, 0, 0, 320, 240);
    this.imageDataURL = this.canvas.nativeElement.toDataURL("image/png");
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
