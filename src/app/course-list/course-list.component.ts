import { Component, OnInit, ViewChild, AfterContentInit } from "@angular/core";
import { MatGridList } from "@angular/material/grid-list";
import { Subject, Observable } from "rxjs";
import { MediaObserver, MediaChange } from "@angular/flex-layout";
import { ApiService } from "../api.service";

export interface Course {
  id: string;
  name: string;
  types: string;
}

@Component({
  selector: "app-course-list",
  templateUrl: "./course-list.component.html",
  styleUrls: ["./course-list.component.css"]
})
export class CourseListComponent implements OnInit, AfterContentInit {
  @ViewChild("grid", { static: true }) grid: MatGridList;

  cols: Subject<any> = new Subject();
  courses: Observable<Course[]>;

  gridByBreakpoint = {
    xl: 4,
    lg: 3,
    md: 3,
    sm: 2,
    xs: 1
  };

  constructor(private observableMedia: MediaObserver, public api: ApiService) {}

  ngOnInit() {
    this.courses = this.api.getAllCourses();
  }

  ngAfterContentInit() {
    this.observableMedia.asObservable().subscribe((change: MediaChange[]) => {
      this.cols.next(this.gridByBreakpoint[change[0].mqAlias]);
    });
  }
}
