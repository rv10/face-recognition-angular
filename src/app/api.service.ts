import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from "../environments/environment";
import { throwError } from "rxjs";
import { Course } from "./course-list/course-list.component";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private API_SERVER = environment.baseUrl;

  constructor(private http: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = "Unknown error!";
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  dataURItoBlob(dataURI) {
    const binary = atob(dataURI.split(",")[1]);
    const array = [];
    for (let i = 0; i < binary.length; i++) {
      array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
      type: "image/jpg"
    });
  }

  signUp(username, firstname, lastname, email, dob, file) {
    const formData = new FormData();
    formData.append("userame", username);
    formData.append("firstname", firstname);
    formData.append("lastname", lastname);
    formData.append("email", email);
    formData.append("dob", dob);
    formData.append("file", file);
    return this.http.post<any>(this.API_SERVER + "/add/user", formData);
  }

  checkUser(username) {
    const formData = new FormData();
    formData.append("userame", username);
    // formData.append("firstname", firstname);
    // formData.append("lastname", lastname);
    // formData.append("email", email);
    // formData.append("dob", dob);
    return this.http.post<any>(this.API_SERVER + "/verify/user", formData);
  }

  verifyImage(username, file) {
    const formData = new FormData();
    formData.append("userame", username);
    formData.append("file", file);
    return this.http.post<any>(this.API_SERVER + "/verify/user/image", formData);
  }

  getAllCourses() {
    return this.http.get<Course[]>(this.API_SERVER + "/coarses");
  }

  getVideoByCourse(id) {
    const formData = new FormData();
    formData.append("corarseId", id);
    return this.http.post<any>(this.API_SERVER + "/videos", formData);
  }

  getQuestionByVideo(id, arr) {
    const formData = new FormData();
    formData.append("videoId", id);
    formData.append("durations", arr);
    return this.http.post<any>(this.API_SERVER + "/questions", formData);
  }
}
