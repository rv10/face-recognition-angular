import {
  Component,
  OnInit,
  ViewChild,
  Renderer2,
  ElementRef,
  Inject,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import { MatVideoComponent } from "mat-video/app/video/video.component";
import { ApiService } from "../api.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";

interface Duration {
  start: any;
  end: any;
}

@Component({
  selector: "app-video-player",
  templateUrl: "./video-player.component.html",
  styleUrls: ["./video-player.component.css"]
})
export class VideoPlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("video", { static: true })
  matVideo: MatVideoComponent;

  @ViewChild("videoCam", { static: false })
  public videoCam: ElementRef;

  @ViewChild("canvas", { static: true })
  canvas: ElementRef;

  private ctx: CanvasRenderingContext2D;

  video: HTMLVideoElement;
  course: any;
  order = 0;
  list = [];
  user: any;
  timeout: any;
  timer = 0;
  dstroyed = false;
  concentration = true;
  duration: Duration[];
  durationIndex;
  halt = false;


  constructor(
    private renderer: Renderer2,
    public api: ApiService,
    public dialog: MatDialog,
    public router: Router,
    private activatedroute: ActivatedRoute
  ) {
    this.course = this.activatedroute.snapshot.paramMap.get("id");
    this.user = localStorage.getItem("user");
    if (!this.user) {
      router.navigateByUrl("/login");
    }
  }

  ngOnInit() {
    this.api.getVideoByCourse(this.course).subscribe(
      data => {
        console.log(data);
        this.list = data;
        this.loadVideo();
      },
      err => {
        console.log(err);
      }
    );
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.videoCam.nativeElement.srcObject = stream;
        this.videoCam.nativeElement.play();
        setTimeout(() => this.verify(), 1000);
      });
    }
  }

  public ngOnDestroy() {
    this.dstroyed = true;
    console.log("destroy webcam");
    this.videoCam.nativeElement.srcObject = null;
    if (this.timeout) { clearInterval(this.timeout); }

  }

  verify() {
    if (this.matVideo.playing) {
      this.api.verifyImage(this.user, this.capture()).subscribe(
        data => {
          console.log(data);
          if (data.status === "Mached") {
            this.timer = 0;
            if (!data.concentration) {
              if (this.concentration) {
                this.duration[this.durationIndex] = { start: "0", end: Math.floor(this.video.duration).toString() };
                this.duration[this.durationIndex].start = Math.floor(this.video.currentTime).toString();
                this.concentration = false;
                console.log(this.video.currentTime);
              }
            } else {
              if (!this.concentration) {
                this.duration[this.durationIndex].end = Math.floor(this.video.currentTime).toString();
                this.durationIndex++;
                this.concentration = true;
                console.log(this.video.currentTime);
              }
            }
          } else {
            this.timer++;
            if (this.timer > 5) {
              this.router.navigateByUrl("/login");
            }
            this.video.pause();
          }
          if (!this.dstroyed) {
            this.verify();
          }
        },
        err => {
          this.router.navigateByUrl("/login");
        }
      );
    } else {
      if (!this.dstroyed) {
        setTimeout(() => this.verify(), 1000);
      }
    }

  }

  public capture() {
    this.ctx = this.canvas.nativeElement.getContext("2d");
    this.canvas.nativeElement.width = this.videoCam.nativeElement.videoWidth;
    this.canvas.nativeElement.height = this.videoCam.nativeElement.videoHeight;
    this.ctx.drawImage(this.videoCam.nativeElement, 0, 0, 640, 480);
    const imageDataURL = this.canvas.nativeElement.toDataURL("image/png");
    const myFile: Blob = this.api.dataURItoBlob(imageDataURL);
    const file = new File([myFile], "imageFileName.png");
    console.log(file);
    return file;
  }

  loadVideo() {
    this.duration = [];
    this.durationIndex = 0;
    if (this.list.length <= this.order) {
      alert("HI! You have successfully completed the course");
      this.router.navigateByUrl("/course");
    }
    this.list.forEach((element, index) => {
      if (this.order === index) {
        this.playVideo(element.name, element.videoPath);
      }
    });
  }
  // "/assets/sample.mkv"
  playVideo(title, src) {
    this.matVideo.src = src;
    this.matVideo.title = title;
    this.video = this.matVideo.getVideoTag();
    this.video.play();
    // this.matVideo.autoplay = true;

    this.renderer.listen(this.video, "ended", () => {
      console.log("video stopped");
      if (!this.halt) {
        this.halt = true;
        this.getQuestions();
      }

    });
  }

  getQuestions() {
    console.log(JSON.stringify(this.duration));
    this.api.getQuestionByVideo(this.list[this.order].videoId, JSON.stringify(this.duration)).subscribe(
      data => {
        console.log(data);
        // if (data.length === 0) {
        //   this.order++;
        //   this.loadVideo();
        // } else {
        this.showQuestion(data);
        this.halt = false;
        // }
      },
      err => {
        console.log(err);
      }
    );
  }

  showQuestion(data) {
    const dialogRef = this.dialog.open(QuestionDialogComponent, {
      // width: "370px",
      // height: "350px",
      disableClose: true,
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
      if (result) {
        this.order++;
        this.loadVideo();
      } else {
        this.loadVideo();
      }
    });
  }
}

@Component({
  selector: "app-question-dialog",
  templateUrl: "question-dialog.html"
})
export class QuestionDialogComponent {
  questions: any;
  answer: any;
  score = 0;

  constructor(
    public dialogRef: MatDialogRef<QuestionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    console.log(data);
    this.questions = data;
  }

  onNext(stepper, index) {
    if (this.answer) {
      console.log(this.answer);
      if (this.questions[index].answer == this.answer) {
        this.score++;
      }
      this.answer = null;
      stepper.next();
    } else {
      console.log("select an answer");
    }
  }

  complete() {
    console.log("complete");
    if (this.score > this.questions.length / 2) {
      this.dialogRef.close(true);
    } else {
      this.dialogRef.close(false);
    }
  }
}
