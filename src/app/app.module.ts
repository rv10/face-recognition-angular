import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatStepperModule } from "@angular/material/stepper";
import { MatRadioModule } from "@angular/material/radio";
import { MatVideoModule } from "mat-video";
import { LoginComponent } from "./login/login.component";
import { VideoLoginComponent } from "./video-login/video-login.component";
import { SignupComponent } from "./signup/signup.component";
import {
  VideoSignupComponent,
  ConfirmDialogComponent
} from "./video-signup/video-signup.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { CourseListComponent } from "./course-list/course-list.component";
import {
  VideoPlayerComponent,
  QuestionDialogComponent
} from "./video-player/video-player.component";
import { QuizComponent } from "./quiz/quiz.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    VideoLoginComponent,
    SignupComponent,
    VideoSignupComponent,
    NotFoundComponent,
    ConfirmDialogComponent,
    CourseListComponent,
    VideoPlayerComponent,
    QuizComponent,
    QuestionDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatGridListModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatVideoModule,
    MatStepperModule,
    MatRadioModule
  ],
  entryComponents: [ConfirmDialogComponent, QuestionDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
