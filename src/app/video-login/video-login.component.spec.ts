import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoLoginComponent } from './video-login.component';

describe('VideoLoginComponent', () => {
  let component: VideoLoginComponent;
  let fixture: ComponentFixture<VideoLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
