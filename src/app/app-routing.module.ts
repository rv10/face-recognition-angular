import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { SignupComponent } from "./signup/signup.component";
import { VideoLoginComponent } from "./video-login/video-login.component";
import { VideoSignupComponent } from "./video-signup/video-signup.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { CourseListComponent } from "./course-list/course-list.component";
import { QuizComponent } from "./quiz/quiz.component";
import { VideoPlayerComponent } from "./video-player/video-player.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "signup", component: SignupComponent },
  { path: "video-login", component: VideoLoginComponent },
  { path: "video-signup", component: VideoSignupComponent },
  { path: "course", component: CourseListComponent },
  { path: "video/:id", component: VideoPlayerComponent },
  { path: "quiz", component: QuizComponent },
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
