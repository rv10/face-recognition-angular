import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "../api.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  login: FormGroup;

  constructor(
    fb: FormBuilder,
    public router: Router,
    public api: ApiService,
    private snackBar: MatSnackBar
  ) {
    this.login = fb.group({
      username: ["", Validators.required]
    });
  }

  onSubmit() {
    console.log(this.login.value.username);
    if (this.login.valid) {
      this.checkUser(this.login.value.username);
    }
  }

  checkUser(user) {
    this.api.checkUser(user).subscribe(
      data => {
        if (data.status) {
          console.log("User Exist");
          this.router.navigateByUrl("/video-login", {
            state: { username: user }
          });
        } else {
          console.log("user does not exist");
          this.openSnackBar("User does not exists", "");
          // this.router.navigateByUrl('/video-signup', { state: { username: this.login.value.username } });
        }
      },
      err => {
        console.warn(err);
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
