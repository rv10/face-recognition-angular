import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "../api.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent {

  login: FormGroup;

  constructor(fb: FormBuilder, public router: Router, public api: ApiService, private snackBar: MatSnackBar) {
    this.login = fb.group({
      username: ["", Validators.required],
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      email: ["", Validators.email],
      dob: ["", Validators.required],

    });
  }

  onSubmit() {
    // console.log(this.login.value.username);

    if (this.login.valid) {
      const { username, firstname, lastname, email, dob } = this.login.value;
      this.api.checkUser(username).subscribe((data) => {
        if (data.status) {
          console.log("User Exist");
          this.openSnackBar("User already exists", "");
        } else {
          this.router.navigateByUrl("/video-signup", { state: { username, firstname, lastname, email, dob } });
        }
      }, (err) => {
        console.warn(err);
        this.openSnackBar("Something went wrong", "");
      });
      // this.router.navigateByUrl('/video-signup', { state: { username: this.login.value.username } });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
